import React from 'react';
import {Switch, Route} from 'react-router-dom';
import HomePage from 'pages/HomePage';
import UserPage from 'pages/UserPage';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route exact path="/users/:id" component={UserPage} />
  </Switch>
);

export default Routes;
