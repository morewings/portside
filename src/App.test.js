import React from 'react';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import store from 'store';
import App from 'App';

describe('App', () => {
  it('renders main page without crashing', () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[{pathname: '/', key: 'mainPage'}]}>
          <App />
        </MemoryRouter>
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  it('renders user details page without crashing', () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter
          initialEntries={[{pathname: '/users/1', key: 'usersPage'}]}>
          <App />
        </MemoryRouter>
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
