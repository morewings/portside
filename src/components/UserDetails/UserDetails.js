import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

class UserDetails extends React.Component {
  componentDidMount() {
    this.props.requestGetSingleUser(this.props.match.params.id);
  }

  render() {
    return (
      <div>
        {this.props.isLoading ? null : (
          <Fragment>
            <div>Name: {this.props.name}</div>
            <div>Email: {this.props.email}</div>
          </Fragment>
        )}
      </div>
    );
  }
}

UserDetails.propTypes = {
  requestGetSingleUser: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  name: PropTypes.string,
  email: PropTypes.string,
  isLoading: PropTypes.bool,
};

UserDetails.defaultProps = {
  name: '',
  email: '',
  isLoading: true,
};

export default UserDetails;
