import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GET_USER} from 'Redux/constants';
import UserDetails from 'components/UserDetails/UserDetails';

const mapStateToProps = ({
  users: {
    userSelected: {name, email},
    isLoading,
  },
}) => ({
  name,
  email,
  isLoading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestGetSingleUser: userId => ({
        type: GET_USER,
        payload: {userId},
      }),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetails);
