import React from 'react';
import {NavLink} from 'react-router-dom';
import {Navbar, Nav, NavItem} from 'reactstrap';

const Navigation = props => (
  <div>
    <Navbar color="light" light expand="md">
      <span className="navbar-brand">Test assignment</span>
      <Nav navbar>
        <NavItem>
          <NavLink className="nav-link" to="/">
            Home
          </NavLink>
        </NavItem>
      </Nav>
    </Navbar>
  </div>
);

export default Navigation;
