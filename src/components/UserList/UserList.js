import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

class UserList extends React.Component {
  componentDidMount() {
    this.props.requestGetAllUsers();
  }

  render() {
    return (
      <div>
        {this.props.isLoading ? null : (
          <ul>
            {this.props.users.map(({name, id}) => (
              <li key={id}>
                <NavLink to={`/users/${id}`}>{name}</NavLink>
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

UserList.propTypes = {
  requestGetAllUsers: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ).isRequired,
};

UserList.defaultProps = {
  isLoading: true,
};

export default UserList;
