import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GET_USERS} from 'Redux/constants';
import UserList from 'components/UserList/UserList';

const mapStateToProps = ({users: {usersAll: users, isLoading}}) => ({
  users,
  isLoading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestGetAllUsers: () => ({
        type: GET_USERS,
      }),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
