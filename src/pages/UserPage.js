import React from 'react';
import UserDetails from 'components/UserDetails/UserDetails.container';

const UserPage = props => (
  <div className="details-page">
    <UserDetails {...props} />
  </div>
);

export default UserPage;
