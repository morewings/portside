import React from 'react';
import UserList from 'components/UserList/UserList.container';

const HomePage = props => (
  <div className="home-page">
    <UserList {...props} />
  </div>
);

export default HomePage;
