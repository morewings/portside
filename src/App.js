import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import Navigation from 'components/Navigation/Navigation';
import Routes from 'Routes';

const App = props => (
  <div className="App">
    <Navigation />
    <Container>
      <Row>
        <Col>
          <Routes />
        </Col>
      </Row>
    </Container>
  </div>
);

export default App;
