import {SAVE_USERS, SAVE_USER, GET_USER, GET_USERS} from 'Redux/constants';

const initialState = {
  usersAll: [],
  userSelected: {},
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_USER:
    case GET_USERS: {
      return {...state, isLoading: true};
    }
    case SAVE_USERS: {
      const {users: usersAll} = action.payload;
      return {...state, usersAll, isLoading: false};
    }
    case SAVE_USER: {
      const {user: userSelected} = action.payload;
      return {...state, userSelected, isLoading: false};
    }
    default: {
      return state;
    }
  }
}
