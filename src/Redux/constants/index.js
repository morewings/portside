export const GET_USERS = 'GET_USERS';

export const SAVE_USERS = 'SAVE_USERS';

export const GET_USER = 'GET_USER';

export const SAVE_USER = 'SAVE_USER';
