import {put, call} from 'redux-saga/effects';
import {get} from 'axios';
import config from 'config';
import {SAVE_USER} from 'Redux/constants';

const getUser = userId => get(`${config.dataUrl}/${userId}`);

export default function* getSingleUser({payload: {userId}}) {
  try {
    const {data: user} = yield call(getUser, userId);
    yield put({type: SAVE_USER, payload: {user}});
  } catch (e) {
    throw new Error(e);
  }
}
