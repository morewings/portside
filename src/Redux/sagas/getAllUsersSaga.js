import {put, call} from 'redux-saga/effects';
import {get} from 'axios';
import config from 'config';
import {SAVE_USERS} from 'Redux/constants';

const getUsers = () => get(config.dataUrl);

export default function* getAllUsers() {
  try {
    const {data: users} = yield call(getUsers);
    yield put({type: SAVE_USERS, payload: {users}});
  } catch (e) {
    throw new Error(e);
  }
}
