import {takeLatest} from 'redux-saga/effects';
import getAllUsers from 'Redux/sagas/getAllUsersSaga';
import getSingleUser from 'Redux/sagas/getSingleUserSaga';
import {GET_USERS, GET_USER} from 'Redux/constants';

function* mySaga() {
  yield takeLatest(GET_USERS, getAllUsers);
  yield takeLatest(GET_USER, getSingleUser);
}

export default mySaga;
